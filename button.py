""" This module implements the Button widget (input/output) and can be used for the GUI"""

import display

class Button:
    """ The constructor of a button object. At least the coordinates should be defined

    |  Args:
    |  x (int): x coordinate
    |  y (int): y coordinate
    |  w (int): width
    |  h (int): height
    """
    def __init__(self, x, y, w, h):
        self.x =  x
        self.y = y
        self.w = w
        self.h = h
        self.updateFlag = False
        self.text = ""
        self.MCallbackList = []
        self.TCallbackList = []

    def MCallbackregister(self, fun):
        """ register functions of the Widget which react to model changes

        |  Args:
        |  fun (function): callback function which is called when the model changes.
        """
        self.MCallbackList.append(fun)
        print("registered to Button MCallback, List content: " + str(self.TCallbackList))

    def TCallbackregister(self, fun):
        """register functions of the Widget which react to Touch input.

        |  Args:
        |  fun (function): callback function which is called when a touch input occurs
        """
        self.TCallbackList.append(fun)
        print("registered to Button TCallback, List content: "+str(self.TCallbackList))

    def containsPt(self, x,y):
        """check if a point is inside of the button margins.

        |  Args:
        |  x (int): x coordinate of the point.
        |  y (int): y coordinate of the point.
        """
        if (x > self.x and x < self.x + self.w and y > self.y and y < self.y + self.h):
            print("button hit")
            return True
        print("no hit")
        return False

    def setUpdate(self):
        """sets the update flag of the button to trigger an update at the next update loop.
        """
        print("Button setting updateFlag")
        self.updateFlag = True

    def getUpdateStatus(self):
        """gets the update flag of the button to determine if an update is pending.
        """
        if (self.updateFlag == True):
            return True
        return False

    def resetUpdateStatus(self):
        """clears the update flag of the button.
        """
        self.updateFlag = False

    def settext(self, str):
        """sets the text which is displayed in the button.

        |  Args:
        |  str (string): button text.
        """
        self.text = str;

    def draw(self):
        """function to draw the button.
        """
        display.draw(self.x, self.y, self.w, self.h, self.text)