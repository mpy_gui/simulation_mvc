"""The model is representing the data of the application backend"""


class Model:
    """  This class implements the application data and can be changed depending on the purpose    """

    def __init__(self):
        self.AppState = 'A'
        self.counter = 1
        self.MCallbackList = []

    def increment(self):
        self.counter += 1
        self.notify()

    def setCounter(self, val):
        self.counter = val
        self.notify()

    def getcount(self):
        return self.counter

    def getAppState(self):
        return self.AppState

    def setAppState(self, a):
        self.AppState = a
        self.notify()

    def notify(self):
        print("Model changed, notifiy Listeners")
        for element in self.MCallbackList:
            element(self.counter)

    def MCallbackregister(self, fun):
        self.MCallbackList.append(fun)
        print("registered to Model MCallbacklist, List content: " + str(self.MCallbackList))