''' Simulation of touch interface '''

import threading
import random
import time

class Touch:
    def __init__(self):
        self.TCallbackList = []
        self.tthr = None

    def TCallbackregister(self, fun):
        self.TCallbackList.append(fun)
        print("registered to IO TcallbackList, List content: "+ str(self.TCallbackList))

    def simTouchEvents(self):
        while (True):
            x = random.randint (0,480)
            y = random.randint (0,272)
            evt = random.randint (0,3)
            print("Touch input x: "+str(x)+" y: "+str(y)+" evt: "+str(evt))
            for element in self.TCallbackList:
                element(x,y,evt)
                time.sleep(1)

    def startTouchSimulation(self):
        self.tthr = threading.Thread(target = self.simTouchEvents)
        self.tthr.start()