# Class to implement the View Logic and to render the view tree

import threading

class View:
    def __init__(self):
        self.updateFlag = False
        self.nodes = []
        self.vthr = None

    def addViewObj(self, obj):
        self.nodes.append(obj)

    def setUpdateFlag(self):
        self.updateFlag = True

    def displayTree(self):
        while (True):
            if (self.updateFlag == True):
                self.updateFlag = False
                for obj in self.nodes:
                    if (obj.getUpdateStatus() == True):
                        print("redrawing object...")
                        obj.resetUpdateStatus()
                        obj.draw()

    def viewStart(self):
        self.vthr = threading.Thread(target=self.displayTree)
        self.vthr.start()
