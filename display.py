''' This module implements the simulation of the display interface and uses the console to print a message which describes what is shown on the display'''

def draw(x,y,w,h,text):
    """ The draw function prints a message to the console which says that an widget is drawn

    |  Args:
    |  x (int): x coordinate
    |  y (int): y corrdinate
    |  w (int): width
    |  h (int): height
    |  text (string): text of the widget
    """
    print("drawing x:"+str(x)+" y:"+str(y)+" w:"+str(w)+" h:"+str(h)+" text: "+text)


'''
from turtle import *
def mydraw():
    color('red', 'yellow')
    begin_fill()
    while True:
        forward(200)
        left(170)
        if abs(pos()) < 1:
            break
    end_fill()
    done()
'''