""" The controller of the MVC Pattern which controls the communication between View and Model """

import button
import view
import model
import myio
import externalmodelmodifier

def main():
	""" This is the documentation of the main function """
	v = view.View()
	t = myio.Touch()
	m = model.Model()
	e = externalmodelmodifier.EModify(m)
	b1 = button.Button(10,20,300,400)
	
	def b1TCallback(x,y,evt):
		""" This is documentation of b1TCallback function """
		if (evt == 0 and b1.containsPt(x,y)):
			m.increment()
	
	def b1MCallback(counter):
		b1.settext(str(counter))
		b1.setUpdate()

	b1.TCallbackregister(b1TCallback)
	b1.MCallbackregister(b1MCallback)

	def MCallback(counter):
		for el in v.nodes:
			for f in el.MCallbackList:
				f(counter)
			v.setUpdateFlag()
	
	def TCallback(x, y, evt):
		for el in v.nodes:
			for f in el.TCallbackList:
				f(x,y,evt)

	t.TCallbackregister(TCallback)
	m.MCallbackregister(MCallback)
	
	v.addViewObj(b1)
	v.setUpdateFlag()
	
	v.viewStart()
	t.startTouchSimulation()
	e.startExtModModifier()

if __name__ == '__main__':
	main()