""" This module imlements a  simulation of an external source which modifies the model data """

import threading
import random
import time

class EModify:
    """ The constructor of a External Modifier

    |  Args:
    |  model (Object of Model class): model which is changed by the modifier

    """
    def __init__(self, model):
        self.model = model

    def modifyModel(self):
        """function which modifies the model in an endless loop with random values. Sleeps after each modification for a while.
        """
        while (True):
            flag = random.randint(0,1)
            if(flag == 1):
                counter = random.randint(0,100)
                print("External source modifies model with value: "+str(counter))
                self.model.setCounter(counter)
            time.sleep(2)

    def startExtModModifier(self):
        """function which starts the modifyModel function in an own thread
        """
        self.exthr = threading.Thread(target=self.modifyModel)
        self.exthr.start()