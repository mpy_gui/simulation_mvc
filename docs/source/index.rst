.. mvc simulation documentation master file, created by
   sphinx-quickstart on Wed Jun  6 16:18:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mvc simulation's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. image:: mvc_pattern.jpg

.. include:: intro.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* `Source code of the Modules <./rtdcodeview/index.html>`_.
