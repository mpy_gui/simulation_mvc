The MVC Pattern for GUI Applications
================================

The MVC Pattern (Model View Controller) is a software pattern which realizes segregation of
the software into the three parts: model, view and controller. Software patterns are used to
create software which is flexible and maintainable. Also porting between di.erent systems can
be facilitated with software patterns. The model contains the data which are managed by the
application. It is independent from the view and from the controller. The MVC pattern contains
the observer pattern which is used in the model. The observers can register themselves to the
model changes. If the model changes, it notifies all the registered observers. The observers can
also been called listeners. The view is used to display the content of the model and for the user
interaction. The view knows how to display the data from the model but is not involved in the
modification of the data. The controller is between the model and the view of the MVC pattern.
The controller administrates the incoming user events which it receives from the view and initiates
the changes of the model and of the view. Utilizing the MVC pattern for the GUI application
facilitate the further development of the project since through the pattern the part which has to
be changed is found quickly.

Implementing a basic MVC Simulation
----------------------------------------------------------------
As the pattern is relatively abstract, it is helpful to implement a very basic application of it
which contains all components. For this reason the model and the view are simplified. The model
contains only one element in the form of a number. The view contains also only one element which
is a button showing the number. By pressing the button the number will be incremented. Using
the MVC pattern this has to occur through the controller. The simulation verifies if the pattern
is applied correctly. To see if the model changes are propagated correctly, there can be included a
further source which can modify the data. The change must be visible then in the view or rather
the text of the button has to change. Figure 4.1 shows the draft of the simulation which consists of
the elements of the MVC pattern. The green box contains the parts which later are implemented
in the hardware and have to be emulated. For the input events there is a thread started which
generates random touch events. Further there is a modifier which can arbitrary change the model.
The display is simulated with printing debugging information to the console. In the other parts of
the MVC structure there are also print statements included to be able to trace how the di.erent
parts work together. With these console outputs the working of the pattern can be verified. An
example for the output is shown in the figure and the simulation is supplied as a PyCharm project
(Folder: simulation_mvc).


Triggering rtd build process via Webhook
----------------------------------------------------------------------
Open the rdt-webinterface of the project and go to admin->webhooks setting and add "incoming git webhook"
copy the link from the webif
Go to the .git project folder in the Git Server
cd to folder hooks
Rename the post-update.sample to post-update
edit post-update and add line with the copied link from the rtd webif:
for example:
exec curl -X POST -d "ref=latest" http://192.168.1.106:8000/api/v2/webhook/httpmvc/1/

(Note that the "exec" statement must be there before the command)

-make changes, commit and wait for the build of the new documentation
